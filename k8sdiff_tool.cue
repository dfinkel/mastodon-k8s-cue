package kube

import (
	"encoding/yaml"
	"tool/exec"
)

command: k8sdiff: {
	task: kube: exec.Run & {
		cmd:   "kubectl diff --context=mastodon --force-conflicts --server-side=true -f -"
		stdin: yaml.MarshalStream(objects)
	}

}
