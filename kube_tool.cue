package kube

objects: [
	//postgres, // We're using CloudSQL since it's cheaper, easier, automatically backed-up and better monitored
	regionalPDStorageStandardClass,
	deployment.redis,
	deployment["mastodon-web"],
	deployment["mastodon-streaming"],
	deployment["mastodon-sidekiq"],
	hpas["mastodon-sidekiq"],
	//serviceaccounts.postgres, // see above
	serviceaccounts.redis,
	serviceaccounts["mastodon-web"],
	serviceaccounts["mastodon-streaming"],
	serviceaccounts["mastodon-sidekiq"],
	services.redis,
	//services.postgres, // see above
	services["mastodon-web"],
	services["mastodon-streaming"],
	gateway,
	gatewayHTTPRoute,
	webfingerHTTPRoute,
	gatewayHTTPRouteWeb,
	gatewayHTTPRouteStreaming,
	mastodonStreamingBEcfg,
	mastodonWebBEcfg,
]
