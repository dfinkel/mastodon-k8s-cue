package kube

// see CRD at _healthcheckCRD for schema (TODO: figure out how to convert that CRD into a cue template/schema)
gatewayHTTPRouteStreaming: {
	apiVersion: "networking.gke.io/v1"
	kind:       "HealthCheckPolicy"
	metadata: {
		name:      "mastodon-streaming"
		namespace: _mastodonNamespace
	}
	spec: {
		default: {
			checkIntervalSec:   5
			timeoutSec:         2
			healthyThreshold:   1
			unhealthyThreshold: 2
			//type:               "HTTP"
			logConfig: enabled: true
			config: {
				httpHealthCheck: {
					portSpecification: "USE_SERVING_PORT"
					requestPath:       "/api/v1/streaming/health"
				}
			}
		}
		targetRef: {
			kind:  "Service"
			name:  services["mastodon-streaming"].metadata.name
			group: ""
		}
	}
}
gatewayHTTPRouteWeb: {
	apiVersion: "networking.gke.io/v1"
	kind:       "HealthCheckPolicy"
	metadata: {
		name:      "mastodon-web"
		namespace: _mastodonNamespace
	}
	spec: {
		default: {
			checkIntervalSec:   5
			timeoutSec:         2
			healthyThreshold:   1
			unhealthyThreshold: 2
			logConfig: enabled: true
			config: {
				httpHealthCheck: {
					portSpecification: "USE_SERVING_PORT"
					requestPath:       "/health"
				}
			}
		}
		targetRef: {
			kind:  "Service"
			name:  services["mastodon-web"].metadata.name
			group: ""
		}
	}
}
