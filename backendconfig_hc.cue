package kube

mastodonWebBEcfg: {
	apiVersion: "cloud.google.com/v1"
	kind:       "BackendConfig"
	metadata: {
		name:      "mastodon-web"
		namespace: _mastodonNamespace
	}
	spec: healthCheck: {
		checkIntervalSec:   6
		timeoutSec:         4
		healthyThreshold:   2
		unhealthyThreshold: 3
		type:               "HTTP"
		requestPath:        "/health"
		port:               _webPort
	}
}

mastodonStreamingBEcfg: {
	apiVersion: "cloud.google.com/v1"
	kind:       "BackendConfig"
	metadata: {
		name:      "mastodon-streaming"
		namespace: _mastodonNamespace
	}
	spec: healthCheck: {
		checkIntervalSec:   6
		timeoutSec:         4
		healthyThreshold:   2
		unhealthyThreshold: 3
		type:               "HTTP"
		requestPath:        "/api/v1/streaming/health"
		port:               _streamingPort
	}
}
