package kube

import (
	corev1 "k8s.io/api/core/v1"
)

deployment: redis: {
	spec: template: {
		spec: containers: [
			_cmdProbe & {
				image: "redis:7-alpine"
				_probeCmd: ["redis-cli", "ping"]
				livenessProbe: exec: command: _probeCmd
			},
		]
		spec: serviceAccountName: serviceaccounts.redis.metadata.name
	}
}

serviceaccounts: redis: {}

services: redis: {
	spec: {
		selector: {app: "redis"}
		ports: [
			{
				name:     "redis"
				protocol: corev1.#ProtocolTCP
				port:     6379
			},
		]
	}
}
