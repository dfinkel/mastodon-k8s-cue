package kube

import (
	"strings"
	"list"

	corev1 "k8s.io/api/core/v1"

	autoscalingv2 "k8s.io/api/autoscaling/v2"
)

_mastodonImg: "tootsuite/mastodon:v4.0.2"

_mastodonGCSEnv: {
	S3_BUCKET:                   "spin-2-mastodon-455c1618117088bac6a2dbb0"
	S3_ENABLED:                  "true"
	S3_PROTOCOL:                 "https"
	S3_HOSTNAME:                 "storage.googleapis.com"
	S3_ENDPOINT:                 "https://storage.googleapis.com"
	_GCS_S3_MULTIPART_THRESHOLD: 50Mi
	S3_MULTIPART_THRESHOLD:      "\(_GCS_S3_MULTIPART_THRESHOLD)"
	S3_REGION:                   "us-central1"
}

_mastodonSecretEnvSources: {
	AWS_ACCESS_KEY_ID: {
		secret: "mastodon-gcs-access"
		key:    "ACCESSID"
	}
	AWS_SECRET_ACCESS_KEY: {
		secret: "mastodon-gcs-access"
		key:    "SECRET"
	}
	SMTP_PASSWORD: {
		secret: "mastodon-smtp-pass"
		key:    "SMTP_PASS"
	}
	SECRET_KEY_BASE: {
		secret: "mastodon-generated"
		key:    "SECRET_KEY_BASE"
	}
	OTP_SECRET: {
		secret: "mastodon-generated"
		key:    "OTP_SECRET"
	}
	VAPID_PRIVATE_KEY: {
		secret: "mastodon-generated"
		key:    "VAPID_PRIVATE_KEY"
	}
	VAPID_PUBLIC_KEY: {
		secret: "mastodon-generated"
		key:    "VAPID_PUBLIC_KEY"
	}
	DB_PASS: _postgresEnvSources.POSTGRES_PASSWORD
}

_mastodonSMTPEnv: {
	SMTP_SERVER:               "smtp-relay.gmail.com"
	SMTP_AUTH_METHOD:          "plain"
	SMTP_CA_FILE:              "/etc/ssl/certs/ca-certificates.crt"
	SMTP_DELIVERY_METHOD:      "smtp"
	SMTP_DOMAIN:               "spin-2.net"
	SMTP_ENABLE_STARTTLS_AUTO: "true"
	SMTP_FROM_ADDRESS:         "mastodon@spin-2.net"
	SMTP_LOGIN:                "mastodon@spin-2.net"
	SMTP_OPENSSL_VERIFY_MODE:  "peer"
	SMTP_PORT:                 "587"
	SMTP_REPLY_TO:             "mastodon@spin-2.net"
	SMTP_SERVER:               "smtp-relay.gmail.com"
	SMTP_TLS:                  "true"
}

_mastodonEnv: {
	DB_HOST:        "10.184.48.3" // cloud SQL instance
	DB_NAME:        "mastodon_production"
	DB_POOL:        "2" // max connections on our CloudSQL instance is 25 (keep it low enough for 3 pods)
	DB_PORT:        "\(_postgresPort)"
	DB_USER:        "mastodon"
	DEFAULT_LOCALE: "en"
	LOCAL_DOMAIN:   "spin-2.net"
	WEB_DOMAIN:     "mastodon.spin-2.net"
	// Registration has been disabled; switch off single-user-mode
	SINGLE_USER_MODE: "true"
	// https://devcenter.heroku.com/articles/tuning-glibc-memory-behavior
	MALLOC_ARENA_MAX:       "2"
	NODE_ENV:               "production"
	RAILS_ENV:              "production"
	REDIS_HOST:             services.redis.metadata.name
	REDIS_PORT:             "6379"
	STREAMING_CLUSTER_NUM:  "1" // should match CPU-limit on streaming pods
	EMAIL_DOMAIN_ALLOWLIST: strings.Join(["spin-2.net"], "|")
	ALTERNATE_DOMAINS:      strings.Join(["mastodon-test.spin-2.net"], ",")
}

_mastodonDeploymentEnvs: [
	for k, v in _mastodonSecretEnvSources {
		name: k
		valueFrom: {
			secretKeyRef: {
				name: v.secret
				key:  v.key
			}
		}
	},
	for k, v in _mastodonSMTPEnv {name: k, value: v},
	for k, v in _mastodonGCSEnv {name:  k, value: v},
	for k, v in _mastodonEnv {name:     k, value: v},
]

_webPort: 3000

deployment: "mastodon-web": {
	spec: template: {
		spec: containers: [
			{
				image: _mastodonImg
				command: ["bundle", "exec", "puma", "-C", "config/puma.rb"]
				livenessProbe: {
					httpGet: {
						path: "/health"
						port: _webPort
					}
				}
				readinessProbe: {
					httpGet: {
						path: "/health"
						port: _webPort
					}
				}
				startupProbe: {
					httpGet: {
						path: "/health"
						port: _webPort
					}
				}
				ports: [{
					name:          "web"
					containerPort: _webPort
					protocol:      corev1.#ProtocolTCP
				}]
				env: list.Concat([[
					{
						name:  "PORT"
						value: "\(_webPort)"
					}],
					_mastodonDeploymentEnvs])
				// Media gets uploaded/converted
				resources: limits: "ephemeral-storage": "10Gi"
			},
		]
		spec: serviceAccountName: serviceaccounts["mastodon-web"].metadata.name
	}
}

serviceaccounts: "mastodon-web": {}

services: "mastodon-web": {
	metadata: annotations: {
		"cloud.google.com/backend-config": "{\"default\": \"\(mastodonWebBEcfg.metadata.name)\"}"
	}
	spec: {
		selector: {app: deployment["mastodon-web"].spec.template.metadata.labels["app"]}
		type: corev1.#ServiceTypeNodePort
		ports: [
			{
				name:        "mastodon-web"
				protocol:    corev1.#ProtocolTCP
				appProtocol: "HTTP"
				targetPort:  _webPort
				port:        _webPort
			},
		]
	}
}

_streamingPort: 4000

deployment: "mastodon-streaming": {
	spec: template: {
		spec: containers: [
			{
				image: _mastodonImg
				command: ["node", "./streaming"]
				livenessProbe: {
					httpGet: {
						path: "/api/v1/streaming/health"
						port: _streamingPort
					}
				}
				readinessProbe: {
					httpGet: {
						path: "/api/v1/streaming/health"
						port: _streamingPort
					}
				}
				startupProbe: {
					httpGet: {
						path: "/api/v1/streaming/health"
						port: _streamingPort
					}
				}
				ports: [{
					name:          "streaming"
					containerPort: _streamingPort
					protocol:      corev1.#ProtocolTCP
				}]
				env: list.Concat([[
					{
						name:  "PORT"
						value: "\(_streamingPort)"
					}],
					_mastodonDeploymentEnvs])
			},
		]
		spec: serviceAccountName: serviceaccounts["mastodon-streaming"].metadata.name
	}
}

serviceaccounts: "mastodon-streaming": {}

services: "mastodon-streaming": {
	metadata: annotations: {
		"cloud.google.com/backend-config": "{\"default\": \"\(mastodonStreamingBEcfg.metadata.name)\"}"
	}
	spec: {
		selector: {app: deployment["mastodon-streaming"].spec.template.metadata.labels["app"]}
		type: corev1.#ServiceTypeNodePort
		ports: [
			{
				name:        "mastodon-streaming"
				protocol:    corev1.#ProtocolTCP
				appProtocol: "HTTP"
				targetPort:  _streamingPort
				port:        _streamingPort
			},
		]
	}
}

deployment: "mastodon-sidekiq": {
	spec: {
		replicas: 1
		template: {
			spec: containers: [
				_cmdProbe & {
					image: _mastodonImg
					command: ["bundle", "exec", "sidekiq", "-c", "2"]
					env: _mastodonDeploymentEnvs
					_probeCmd: ["/bin/true"]
					livenessProbe: exec: command: _probeCmd
					// Media gets uploaded/converted
					resources: limits: {
						"cpu":               "250m" // minimum allowed by GKE autopilot
						"ephemeral-storage": "10Gi"
					}
				},
			]
			spec: serviceAccountName: serviceaccounts["mastodon-sidekiq"].metadata.name
			spec: nodeSelector: {"cloud.google.com/gke-spot": "true"}
		}
	}
}

serviceaccounts: "mastodon-sidekiq": {}

hpas: "mastodon-sidekiq": {
	spec: {
		scaleTargetRef: {
			kind:       deployment["mastodon-sidekiq"].kind
			name:       deployment["mastodon-sidekiq"].metadata.name
			apiVersion: deployment["mastodon-sidekiq"].apiVersion
		}
		maxReplicas: 2
		metrics: [{
			type: autoscalingv2.#ResourceMetricSourceType
			resource: {
				name: "cpu"
				target: {
					type:               autoscalingv2.#UtilizationMetricType
					averageUtilization: 89
				}
			}
		}]
		behavior: {
			scaleUp: {
				stabilizationWindowSeconds: 420 // wait 7 minutes
				policies: [
					{
						type:          autoscalingv2.#PodsScalingPolicy
						value:         1
						periodSeconds: 60
					},
				]
			}
			scaleDown: {
				stabilizationWindowSeconds: 120 // wait 2 minutes
				policies: [
					{
						type:          autoscalingv2.#PodsScalingPolicy
						value:         1
						periodSeconds: 60
					},
				]
			}
		}
	}
}
