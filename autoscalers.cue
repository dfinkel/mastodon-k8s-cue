package kube

import (
	autoscalingv2 "k8s.io/api/autoscaling/v2"
)

hpas: [ID=_]: autoscalingv2.#HorizontalPodAutoscaler & {
	apiVersion: "autoscaling/v2"
	kind:       "HorizontalPodAutoscaler"
	_name:      ID
	metadata: {
		name:      _name
		namespace: _mastodonNamespace
	}
	spec: {
		minReplicas: *1 | int32
		maxReplicas: *2 | int32 & >minReplicas
	}
}
