package kube

import (
	appsv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	storagev1 "k8s.io/api/storage/v1"
)

_mastodonNamespace: "mastodon"

_postgresEnvSources: {
	POSTGRES_PASSWORD: {
		secret: "db-pass"
		key:    "PASS"
	}
}

_postgresEnv: {
	POSTGRES_DB:   _mastodonEnv.DB_NAME
	POSTGRES_USER: _mastodonEnv.DB_USER
	//"POSTGRES_HOST_AUTH_METHOD": "trust"
}

_postgresPort: 5432

services: postgres: {
	spec: {
		selector: {app: "db"}
		ports: [
			{
				name:        "postgres"
				protocol:    corev1.#ProtocolTCP
				appProtocol: "postgres"
				port:        _postgresPort
			},
		]
	}
}

serviceaccounts: postgres: {}

postgres: appsv1.#StatefulSet & _spec & {
	kind:       "StatefulSet"
	apiVersion: "apps/v1"
	metadata: {
		name:      "postgres"
		namespace: _mastodonNamespace
	}
	_name: "postgres"

	spec: {
		replicas: 1
		let podLabels = selector.matchLabels
		selector: {
			matchLabels: {app: "postgres", service: "postgres"}
		}
		template: {
			metadata: {
				labels: podLabels
			}
			spec: {
				containers: [_cmdProbe & {
					name:  "postgres"
					image: "postgres:14-alpine"
					env: [
						for k, v in _postgresEnvSources {
							name: k
							valueFrom: {
								secretKeyRef: {
									name: v.secret
									key:  v.key
								}
							}
						},
						for k, v in _postgresEnv {name: k, value: v},
					]
					volumeMounts: [{
						name:      volumeClaimTemplates[0].metadata.name
						mountPath: "/var/lib/postgresql/data"
					}]
					_probeCmd: ["pg_isready", "-U", "postgres"]
					livenessProbe: exec: command: _probeCmd
				},
				]
				serviceAccountName: serviceaccounts.postgres.metadata.name
			}
		}
		serviceName:         services.postgres.metadata.name
		podManagementPolicy: appsv1.#OrderedReadyPodManagement
		updateStrategy: {
			type: appsv1.#RollingUpdateStatefulSetStrategyType
			rollingUpdate: {
				maxUnavailable: 1
			}
		}
		minReadySeconds: 10
		persistentVolumeClaimRetentionPolicy: {
			whenDeleted: appsv1.#RetainPersistentVolumeClaimRetentionPolicyType
			whenScaled:  appsv1.#RetainPersistentVolumeClaimRetentionPolicyType
		}
		volumeClaimTemplates: [{
			metadata: {
				name: "postgres-data"
			}
			spec: {
				accessModes: [corev1.#ReadWriteOnce]
				storageClassName: regionalPDStorageStandardClass.metadata.name
				resources: {
					limits: {
						"storage": "200Gi"
					}
					requests: limits
				}
			}
		}]
	}
}

regionalPDStorageStandardClass: storagev1.#StorageClass & {
	kind:       "StorageClass"
	apiVersion: "storage.k8s.io/v1"
	metadata: {
		name: "regional-pd-standard"
		// not constrained to a specific namespace
		annotations: {
			"components.gke.io/layer": "addon"
		}
		labels: {
			"addonmanager.kubernetes.io/mode": "EnsureExists"
			"k8s-app":                         "gcp-compute-persistent-disk-csi-driver"
		}
	}
	provisioner:       "pd.csi.storage.gke.io"
	reclaimPolicy:     corev1.#PersistentVolumeReclaimRetain
	volumeBindingMode: storagev1.#VolumeBindingWaitForFirstConsumer
	parameters: {
		type:               "pd-standard"
		"replication-type": "regional-pd"
	}
	allowVolumeExpansion: true
}
