package kube

import (
	corev1 "k8s.io/api/core/v1"
)

serviceaccounts: [ID=_]: corev1.#ServiceAccount & {
	kind:       "ServiceAccount"
	apiVersion: "v1"

	metadata: {
		name:      ID
		namespace: _mastodonNamespace
	}
	automountServiceAccountToken: false
}
