package kube

import (
	"encoding/yaml"
	"tool/exec"
)

command: apply: {
	task: kube: exec.Run & {
		cmd:    "kubectl apply --context=mastodon --force-conflicts --server-side=true -f -"
		stdin:  yaml.MarshalStream(objects)
		stdout: string
	}

}
