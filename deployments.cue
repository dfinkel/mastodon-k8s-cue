package kube

import (
	appsv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
)

deployment: [ID=_]: _spec & appsv1.#Deployment & {
	apiVersion: "apps/v1"
	kind:       "Deployment"
	_name:      ID
	spec: {
		replicas: *1 | int | null
		strategy: {
			type: appsv1.#RollingUpdateDeploymentStrategyType
			rollingUpdate: {
				maxUnavailable: *0 | int
				maxSurge:       *1 | int
			}
		}
	}
}

_spec: {
	_name: string

	metadata: name:      _name
	metadata: namespace: _mastodonNamespace
	spec: selector: matchLabels: {app: _name}
	spec: template: {
		metadata: labels: {
			app: _name
		}
		spec: containers: [{
			name:            _name
			imagePullPolicy: corev1.#PullIfNotPresent
			resources: {
				limits: {
					cpu:                 *"250m" | string
					memory:              *"1Gi" | string
					"ephemeral-storage": *"10Mi" | string
				}
				requests: limits // Guaranteed QoS
			}
			livenessProbe: {
				initialDelaySeconds: *1 | int
				timeoutSeconds:      *4 | int
				periodSeconds:       *10 | int
				successThreshold:    *1 | int
				failureThreshold:    *10 | int
			}
			readinessProbe: {
				initialDelaySeconds: *1 | int
				timeoutSeconds:      *2 | int
				periodSeconds:       *5 | int
				successThreshold:    *1 | int
				failureThreshold:    *10 | int
			}
			startupProbe: {
				initialDelaySeconds: *0 | int
				timeoutSeconds:      *5 | int
				periodSeconds:       *1 | int
				successThreshold:    *1 | int
				failureThreshold:    *900 | int
			}
		}, ...]
		spec: restartPolicy: corev1.#RestartPolicyAlways
	}
}

_cmdProbe: corev1.#Container & {
	let _probeCmd = livenessProbe.exec.command
	livenessProbe: {
		exec: {}
	}
	readinessProbe: {
		exec: {
			command: _probeCmd
		}
	}
	startupProbe: {
		exec: {
			command: _probeCmd
		}
	}
}
