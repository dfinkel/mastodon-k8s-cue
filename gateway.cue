package kube

import (
	gateway_v1beta1 "sigs.k8s.io/gateway-api/apis/v1beta1"
)

gateway: gateway_v1beta1.#Gateway & {
	kind:       "Gateway"
	apiVersion: "gateway.networking.k8s.io/v1beta1"
	metadata: {
		name:      "mastodon"
		namespace: _mastodonNamespace
		annotations: {
			"networking.gke.io/certmap": "mastodon-spin-2-net"
		}
	}
	spec: {
		gatewayClassName: "gke-l7-gxlb"
		listeners: [{
			name:     "https"
			protocol: "HTTPS"
			port:     443
		}]
	}
}

gatewayHTTPRoute: gateway_v1beta1.#HTTPRoute & {
	apiVersion: "gateway.networking.k8s.io/v1beta1"
	kind:       "HTTPRoute"
	metadata: {
		name:      "mastodon"
		namespace: _mastodonNamespace
	}
	spec: {
		parentRefs: [
			{
				kind:      gateway.kind
				name:      gateway.metadata.name
				namespace: _mastodonNamespace
			},
		]
		hostnames: [
			"mastodon.spin-2.net",
			"mastodon-test.spin-2.net",
		]
		rules: [{
			matches: [
				{path: value: "/"},
			]
			backendRefs: [
				{
					kind:      services["mastodon-web"].kind
					name:      services["mastodon-web"].metadata.name
					namespace: services["mastodon-web"].metadata.namespace
					port:      services["mastodon-web"].spec.ports[0].port
				},
			]
		}, {
			matches: [
				{path: value: "/api/v1/streaming"},
			]
			backendRefs: [
				{
					kind:      services["mastodon-streaming"].kind
					name:      services["mastodon-streaming"].metadata.name
					namespace: services["mastodon-streaming"].metadata.namespace
					port:      services["mastodon-streaming"].spec.ports[0].port
				},
			]
		},
		]
	}
}

webfingerHTTPRoute: gateway_v1beta1.#HTTPRoute & {
	apiVersion: "gateway.networking.k8s.io/v1beta1"
	kind:       "HTTPRoute"
	metadata: {
		name:      "webfinger"
		namespace: _mastodonNamespace
	}
	spec: {
		parentRefs: [
			{
				kind:      gateway.kind
				name:      gateway.metadata.name
				namespace: _mastodonNamespace
			},
		]
		hostnames: [
			"spin-2.net",
		]
		rules: [{
			matches: [
				{path: value: "/.well-known/webfinger"},
			]
			backendRefs: [
				{
					kind:      services["mastodon-web"].kind
					name:      services["mastodon-web"].metadata.name
					namespace: services["mastodon-web"].metadata.namespace
					port:      services["mastodon-web"].spec.ports[0].port
				},
			]
		},
		]
	}
}
