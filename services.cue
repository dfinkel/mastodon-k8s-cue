package kube

import (
	corev1 "k8s.io/api/core/v1"
)

services: [ID=_]: corev1.#Service & {
	kind:       "Service"
	apiVersion: "v1"
	metadata: {
		name:      ID
		namespace: _mastodonNamespace
	}
	spec: {
		type: *corev1.#ServiceTypeClusterIP | corev1.#ServiceTypeNodePort |
					corev1.#ServiceTypeLoadBalancer | corev1.#ServiceTypeExternalName
		internalTrafficPolicy: corev1.#ServiceInternalTrafficPolicyCluster
	}
}
